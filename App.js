/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, Button, StyleSheet, Text, View} from 'react-native';
// import MusicControl from 'react-native-music-control';
const Sound = require('react-native-sound');
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

import Video from 'react-native-video';

type Props = {};
export default class App extends Component<Props> {

  componentDidMount() {
    // MusicControl.enableBackgroundMode(true);
  }

  play() {


// Enable playback in silence mode
Sound.setCategory('Playback');

// Load the sound file 'whoosh.mp3' from the app bundle
// See notes below about preloading sounds within initialization code below.
var whoosh = new Sound(require('./Comfort_Fit_-_03_-_Sorry.mp3'), (error) => {
  if (error) {
    alert('failed to load the sound', error);
    return;
  }
  // loaded successfully
  alert('duration in seconds: ' + whoosh.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
  whoosh.play((success) => {
    alert(success)
    if (success) {
      alert('successfully finished playing');
    } else {
      alert('playback failed due to audio decoding errors');
      // reset the player to its uninitialized state (android only)
      // this is the only option to recover after an error occured and use the player again
      whoosh.reset();
    }
  });
});

// Play the sound with an onEnd callback

    // const soundObject = new Expo.Audio.Sound();
    // try {
    //   await soundObject.loadAsync(require('./Comfort_Fit_-_03_-_Sorry.mp3'));
    // { shouldPlay: true }
    //   this.audioPlayer1  = soundObject;
    //     this.audioPlayer1.playAsync();
    //     this.audioPlayer1.setPositionAsync(0);
    //     this.audioPlayer1.setRateAsync(3, false);
    // // Your sound is playing!
    // } catch (error) {
    // // An error occurred!

    // }
  }
  render() {
    return (
      <View style={styles.container}>
          <Video
            source={require("./Comfort_Fit_-_03_-_Sorry.mp3")}
            ref={(ref) => {
              this.player = ref
            }}
            playInBackground={true}
            ignoreSilentSwitch="ignore"
          />
        {/* <Button
          onPress={this.play.bind(this)}
          title="Play"
          color="#841584"
          accessibilityLabel="Learn"
        /> */}
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
